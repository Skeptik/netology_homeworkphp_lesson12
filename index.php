<?php
try{
    $db = new PDO("mysql:host=localhost;dbname=global", "yabanji", "neto1643", array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
} catch (PDOException $e){
    die("Error: ".$e->getMessage());
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
    table {
        border-spacing: 0;
        border-collapse: collapse;
    }

    table td, table th {
        border: 1px solid #ccc;
        padding: 5px;
    }

    table th {
        background: #eee;
    }
    div {
        padding: 7px;
        padding-right: 20px;
        border: solid 1px black;
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 13pt;
        background: #E6E6FA;
       }
</style>
</head>
<body>

<h1>Библиотека успешного человека</h1>

<div>
<form method="GET" action="#" name="myform">
    <input type="text" name="isbn" placeholder="ISBN" value="" />
    <input type="text" name="name" placeholder="Название книги" value="" />
    <input type="text" name="author" placeholder="Автор книги" value="" />
    <input type="submit" value="Поиск" />
</form>
</div>
<?php
    try{
        if (!empty($_GET['name'])){
            $check = "SELECT * FROM books WHERE name='".$_GET['name']."'";
            $results = $db->query($check);
        } elseif (!empty($_GET['isbn'])){
            $check = "SELECT * FROM books WHERE isbn='".$_GET['isbn']."'";
            $results = $db->query($check);
        } elseif (!empty($_GET['author'])){
            $check = "SELECT * FROM books WHERE author='".$_GET['author']."'";
            $results = $db->query($check);
        }else{
            $results = $db->query("SELECT * FROM books");
        }
        $error_array = $db->errorInfo();
        if($db->errorCode() != 0000){
            echo "SQL ошибка ".$error_array[2].'<br>';
            die("Error");
        }
    } catch (PDOException $e){
        die("Error: ".$e->getMessage());
    }
?>
    <table>
      <tr>
        <th>Название</th>
        <th>Автор</th>
        <th>Год выпуска</th>
        <th>Жанр</th>
        <th>ISBN</th>
      </tr>
<?php
    while ($row = $results->fetch(PDO::FETCH_ASSOC)) {
?>
        <tr>
          <td><?= $row['name'] ?></td>
          <td><?= $row['author'] ?></td>
          <td><?= $row['year'] ?></td>
          <td><?= $row['genre'] ?></td>
          <td><?= $row['isbn'] ?></td>
        </tr>
<?php
    }
?>
</table>
</body>
</html>